// tslint:disable-next-line:no-implicit-dependencies
import { configure } from "enzyme"
// tslint:disable-next-line:no-implicit-dependencies
import * as EnzymeAdapter from "enzyme-adapter-react-16"
configure({ adapter: new EnzymeAdapter() })
