import * as React from "react"
import { hot } from "react-hot-loader"
import { Route, Switch } from "react-router"

import BlogList from "app/BlogList"
import EditPost from "app/EditPost"

import "./global.css"

export const App = hot(module)(() => (
  <Switch>
    <Route exact path="/" component={BlogList} />
    <Route path="/create" component={EditPost} />
    <Route path="/edit/:postId" component={EditPost} />
  </Switch>
))
