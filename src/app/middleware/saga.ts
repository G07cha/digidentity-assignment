import createSagaMiddleware from "redux-saga"

import postSaga from "app/shared/ducks/post/sagas"

export const sagaMiddleware = createSagaMiddleware()

export const runSagas = () => {
  sagaMiddleware.run(postSaga)
}
