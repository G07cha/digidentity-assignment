import { push } from "connected-react-router"
import * as React from "react"
import { connect } from "react-redux"
import { RouteComponentProps } from "react-router"

import { Button } from "app/shared/components/Button"
import { Input } from "app/shared/components/Input"
import { Textarea } from "app/shared/components/Textarea"
import { actions, selectors } from "app/shared/ducks/post"
import { IPost } from "app/shared/models"
import { RootState } from "app/shared/store/State"

import * as style from "./style.css"

type IOwnProps = RouteComponentProps<{
  postId?: string
}>

interface IStateProps {
  post?: IPost
}

interface IDispatchProps {
  getPost(payload: actions.IGetPostPayload): void
  createPost(payload: actions.ICreatePostPayload): void
  updatePost(payload: actions.IUpdatePostPayload): void
  goToBlogList(): void
}

export type Props = IOwnProps & IStateProps & IDispatchProps

export interface IState {
  form: IPost
}

export class EditPost extends React.Component<Props, IState> {
  constructor(props: Props) {
    super(props)
    this.state = {
      form: props.post || {
        id: -1,
        userId: -1,
        title: "",
        body: "",
      },
    }
  }
  public componentWillReceiveProps(nextProps: Props) {
    this.setState({
      form: nextProps.post ? nextProps.post : this.state.form,
    })
  }

  public componentWillMount() {
    const { match, post, getPost } = this.props
    const id = match.params.postId && parseInt(match.params.postId, 10)

    if (id && !post) {
      getPost({ id })
    }
  }

  public render() {
    const { form } = this.state
    const { goToBlogList } = this.props

    return (
      <div className={style.editPost}>
        <h4>
          {this.isNewPost() ? "Create" : "Edit"} post{" "}
          <Button onClick={goToBlogList}>Go back</Button>
        </h4>
        <form onSubmit={this.createOrUpdatePost}>
          <Input
            label="Title"
            name="title"
            required
            value={form.title}
            onChange={this.updateTitle}
          />
          <Textarea
            label="Body"
            name="body"
            required
            value={form.body}
            onChange={this.updateBody}
          />
          <Button type={Button.Type.Primary}>{this.isNewPost() ? "Create" : "Update"}</Button>
        </form>
      </div>
    )
  }

  private isNewPost = () => !this.props.match.params.postId

  private updateTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target
    this.setState((oldState) => ({
      form: {
        ...oldState.form,
        title: value,
      },
    }))
  }

  private updateBody = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const { value } = event.target
    this.setState((oldState) => ({
      form: {
        ...oldState.form,
        body: value,
      },
    }))
  }

  private createOrUpdatePost = (event: React.FormEvent) => {
    const { createPost, updatePost, goToBlogList } = this.props

    event.preventDefault()

    if (this.isNewPost()) {
      createPost(this.state.form)
    } else {
      updatePost(this.state.form)
    }

    goToBlogList()
  }
}

export default connect<IStateProps, IDispatchProps, IOwnProps>(
  (state: RootState, { match }) => ({
    post: match.params.postId
      ? selectors.getPost(state, parseInt(match.params.postId, 10))
      : undefined,
  }),
  (dispatch) => ({
    getPost: (payload) => dispatch(actions.getPost(payload)),
    createPost: (payload) => dispatch(actions.createPost(payload)),
    updatePost: (payload) => dispatch(actions.updatePost(payload)),
    goToBlogList: () => dispatch(push("/")),
  }),
)(EditPost)
