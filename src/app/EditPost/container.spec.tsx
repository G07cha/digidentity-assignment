// tslint:disable-next-line:no-implicit-dependencies
import { shallow } from "enzyme"
import * as React from "react"

import { getPost } from "app/__mocks__/post.mock"
import { getMockRouterProps } from "app/__mocks__/RouteComponentProps.mock"
import { EditPost, Props } from "./container"

const routerProps = getMockRouterProps<{ postId: string }>({ postId: "1" })
const mockProps: Props = {
  ...routerProps,
  post: getPost(),
  getPost: jest.fn(),
  createPost: jest.fn(),
  updatePost: jest.fn(),
  goToBlogList: jest.fn(),
}

describe("BlogList", () => {
  it("should render given items", () => {
    expect(shallow(<EditPost {...mockProps} />)).toMatchSnapshot()
  })
})
