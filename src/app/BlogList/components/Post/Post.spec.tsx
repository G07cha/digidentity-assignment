// tslint:disable-next-line:no-implicit-dependencies
import { shallow } from "enzyme"
import * as React from "react"

import { getPost } from "app/__mocks__/post.mock"
import { Post, Props } from "./Post"

const mockProps: Props = {
  onDelete: jest.fn(),
  post: getPost(),
}

describe("Post", () => {
  it("should render given post", () => {
    expect(shallow(<Post {...mockProps} />)).toMatchSnapshot()
  })

  it("should call deletePost when delete button is clicked", () => {
    const wrapper = shallow(<Post {...mockProps} />)

    wrapper.find('[data-test-id="delete-btn"]').simulate("click")

    expect(mockProps.onDelete).toHaveBeenCalled()
  })
})
