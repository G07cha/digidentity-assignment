import * as React from "react"
import { Link } from "react-router-dom"

import { Button } from "app/shared/components/Button"
import { IPost } from "app/shared/models"

import * as style from "./style.css"

interface IOwnProps {
  post: IPost
  onDelete: (post: IPost) => void
}

export type Props = IOwnProps

export class Post extends React.Component<Props> {
  public render() {
    const { post } = this.props

    return (
      <div className={style.post}>
        <h4>
          <span className={style.postHeader}>{post.title}</span>
          <div className={style.postActions}>
            <Button data-test-id="delete-btn" onClick={this.handleDelete} type={Button.Type.Danger}>
              Delete
            </Button>
            <Link to={`/edit/${post.id}`}>
              <Button>Edit</Button>
            </Link>
          </div>
        </h4>
        <p>{post.body}</p>
      </div>
    )
  }

  private handleDelete = () => {
    this.props.onDelete(this.props.post)
  }
}
