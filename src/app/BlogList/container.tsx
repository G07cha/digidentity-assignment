import * as React from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"

import { Button } from "app/shared/components/Button"
import { InfiniteScroll } from "app/shared/components/InifiniteScroll"
import { actions, selectors } from "app/shared/ducks/post"
import { IPost } from "app/shared/models"
import { RootState } from "app/shared/store/State"
import { Post } from "./components/Post"

interface IStateProps {
  posts: ReadonlyArray<IPost>
  isLoading: boolean
}

interface IDispatchProps {
  getPosts(payload: actions.IGetPostsPayload): void
  deletePost(payload: actions.IDeletePostPayload): void
}

export type Props = IStateProps & IDispatchProps

export class BlogList extends React.Component<Props> {
  private currentPage = 0
  private readonly itemPerPage = 10

  public componentWillMount() {
    if (this.props.posts.length < this.itemPerPage) {
      this.loadNextPage()
    }
  }

  public render() {
    const { posts, isLoading } = this.props

    return (
      <div>
        <Link to="/create">
          <Button type={Button.Type.Primary}>Add post</Button>
        </Link>
        <hr />
        <InfiniteScroll threshold={100} loadMore={this.loadNextPage}>
          {posts.map((post) => (
            <Post post={post} key={post.id} onDelete={this.deletePost} />
          ))}
          {isLoading && <h4>Loading...</h4>}
        </InfiniteScroll>
      </div>
    )
  }

  private loadNextPage = () => {
    if (this.props.isLoading) {
      return
    }

    this.currentPage++
    this.props.getPosts({
      limit: this.itemPerPage,
      page: this.currentPage,
    })
  }

  private deletePost = ({ id }: IPost) => {
    this.props.deletePost({ id })
  }
}

export default connect<IStateProps, IDispatchProps>(
  (state: RootState) => ({
    posts: selectors.getPosts(state),
    isLoading: selectors.isLoadingPosts(state),
  }),
  (dispatch) => ({
    getPosts: (payload) => dispatch(actions.getPosts(payload)),
    deletePost: (payload) => dispatch(actions.deletePost(payload)),
  }),
)(BlogList)
