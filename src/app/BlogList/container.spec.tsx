// tslint:disable-next-line:no-implicit-dependencies
import { shallow } from "enzyme"
import * as React from "react"

import { getPost } from "app/__mocks__/post.mock"
import { BlogList, Props } from "./container"

const mockProps: Props = {
  posts: new Array(10).fill(getPost()),
  isLoading: false,
  getPosts: jest.fn(),
  deletePost: jest.fn(),
}

describe("BlogList", () => {
  beforeEach(() => {
    ;(mockProps.getPosts as any).mockClear()
  })

  it("should render given items", () => {
    expect(shallow(<BlogList {...mockProps} />)).toMatchSnapshot()
  })

  it("should call getPosts on mount", () => {
    shallow(<BlogList {...mockProps} posts={[]} />)

    expect(mockProps.getPosts).toHaveBeenCalled()
  })

  it("should not call getPosts on mount if there is already list of posts", () => {
    shallow(<BlogList {...mockProps} />)

    expect(mockProps.getPosts).not.toHaveBeenCalled()
  })
})
