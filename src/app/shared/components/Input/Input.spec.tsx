// tslint:disable-next-line:no-implicit-dependencies
import { shallow } from "enzyme"
import * as React from "react"

import { Input } from "./Input"

describe("Input", () => {
  it("should render content", () => {
    expect(shallow(<Input name="test-name" label="Test label" value="" />)).toMatchSnapshot()
  })
})
