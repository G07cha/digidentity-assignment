import * as React from "react"

interface IOwnProps {
  loadMore: () => void
}

interface IDefaultProps {
  threshold: number
}

export type Props = IOwnProps & Partial<IDefaultProps>

type PropsWithDefaults = Props & IDefaultProps

export class InfiniteScroll extends React.Component<PropsWithDefaults> {
  public static defaultProps: IDefaultProps = {
    threshold: 0,
  }
  private node: HTMLElement | null = null

  public componentWillMount() {
    window.addEventListener("scroll", this.handleScroll)
  }

  public componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll)
  }

  public render() {
    return React.createElement("div", {
      ref: this.setNode,
      children: this.props.children,
    })
  }

  private setNode = (node: HTMLElement | null) => {
    this.node = node
  }

  private calculateTopPosition(el: HTMLElement | null): number {
    if (!el) {
      return 0
    }
    return el.offsetTop + this.calculateTopPosition(el.offsetParent as HTMLElement)
  }

  private calculateNodeOffset(scrollTop: number) {
    if (!this.node) {
      return 0
    }

    return (
      this.calculateTopPosition(this.node) +
      (this.node.offsetHeight - scrollTop - window.innerHeight)
    )
  }

  private handleScroll = () => {
    const offset = this.calculateNodeOffset(window.pageYOffset)

    if (offset < this.props.threshold && (this.node && this.node.offsetParent !== null)) {
      this.props.loadMore()
    }
  }
}
