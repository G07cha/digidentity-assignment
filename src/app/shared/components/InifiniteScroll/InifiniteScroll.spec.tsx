// tslint:disable-next-line:no-implicit-dependencies
import "jsdom-global/register"

// tslint:disable-next-line:no-implicit-dependencies
import { mount } from "enzyme"
import * as React from "react"

import { InfiniteScroll } from "./InfiniteScroll"

describe("InfiniteScroll", () => {
  it("should render given content", () => {
    expect(
      mount(
        <InfiniteScroll loadMore={jest.fn()}>
          <span>Foo</span>
          <span>Bar</span>
          <span>Baz</span>
        </InfiniteScroll>,
      ),
    ).toMatchSnapshot()
  })
})
