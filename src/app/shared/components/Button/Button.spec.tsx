// tslint:disable-next-line:no-implicit-dependencies
import { shallow } from "enzyme"
import * as React from "react"

import { Button } from "./Button"

describe("Button", () => {
  it("should render content", () => {
    expect(shallow(<Button type={Button.Type.Primary}>Test</Button>)).toMatchSnapshot()
  })
})
