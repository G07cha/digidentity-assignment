import classnames from "classnames"
import * as React from "react"

import * as style from "./style.css"

enum Type {
  Primary = "primary",
  Secondary = "secondary",
  Danger = "danger",
}

interface IDefaultProps {
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void
  type: Type
}

export type Props = Partial<IDefaultProps>

type PropsWithDefaults = Props & IDefaultProps

export class Button extends React.PureComponent<PropsWithDefaults> {
  public static Type = Type
  public static defaultProps: IDefaultProps = {
    onClick: () => {
      /* noop */
    },
    type: Button.Type.Secondary,
  }

  public render() {
    const { children, type, onClick } = this.props

    return (
      <button className={classnames(style.button, style[type])} onClick={onClick}>
        {children}
      </button>
    )
  }
}
