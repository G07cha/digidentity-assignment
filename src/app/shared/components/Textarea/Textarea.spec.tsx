// tslint:disable-next-line:no-implicit-dependencies
import { shallow } from "enzyme"
import * as React from "react"

import { Textarea } from "./Textarea"

describe("Input", () => {
  it("should render content", () => {
    expect(shallow(<Textarea name="test-name" label="Test label" value="" />)).toMatchSnapshot()
  })
})
