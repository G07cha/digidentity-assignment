import * as React from "react"

import * as style from "./style.css"

interface IOwnProps {
  value: string
}

interface IDefaultProps {
  onChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => void
  required: boolean
  name: string
  label: string
}

export type Props = IOwnProps & Partial<IDefaultProps>

type PropsWithDefaults = Props & IDefaultProps

export class Textarea extends React.PureComponent<PropsWithDefaults> {
  public static defaultProps: IDefaultProps = {
    onChange: () => {
      /* noop */
    },
    required: false,
    name: "",
    label: "",
  }

  public render() {
    const { required, name, label, onChange, value } = this.props

    return (
      <div>
        {label && (
          <label className={style.label} htmlFor={name}>
            {label}
          </label>
        )}
        <textarea
          className={style.textarea}
          name={name}
          required={required}
          onChange={onChange}
          value={value}
        />
      </div>
    )
  }
}
