import { RouterState } from "connected-react-router"
import { combineReducers } from "redux"

import { postsReducer } from "app/shared/ducks/post/reducer"
import { RootState } from "./State"

export { RootState, RouterState }

// NOTE: current type definition of Reducer in 'connected-react-router' and 'redux-actions' module
// doesn't go well with redux@4
export const rootReducer = combineReducers({
  posts: postsReducer as any,
})
