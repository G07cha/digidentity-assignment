import { IPost } from "app/shared/models/Post"
import { DeepReadonly } from "app/shared/utils/types"
import { RouterState } from "connected-react-router"

interface IState {
  posts: {
    items: IPost[]
    isLoading: boolean
    error: string | Error,
  }
  router: RouterState
}

export type RootState = DeepReadonly<IState>
