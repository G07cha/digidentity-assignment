import { connectRouter, routerMiddleware } from "connected-react-router"
import { History } from "history"
import { AnyAction, applyMiddleware, createStore, Store } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"

import { logger, runSagas, sagaMiddleware } from "app/middleware"
import { rootReducer, RootState } from "./rootReducer"

export function configureStore(history: History, initialState?: RootState): Store<RootState> {
  let middleware = applyMiddleware(logger, routerMiddleware(history), sagaMiddleware)

  if (process.env.NODE_ENV !== "production") {
    middleware = composeWithDevTools(middleware)
  }

  const store = createStore<RootState, AnyAction, {}, {}>(
    connectRouter(history)(rootReducer) as any,
    initialState as RootState,
    middleware,
  )

  runSagas()

  if (module.hot) {
    module.hot.accept("./rootReducer", () => {
      const nextReducer = require("./rootReducer")
      store.replaceReducer(connectRouter(history)(nextReducer))
    })
  }

  return store
}
