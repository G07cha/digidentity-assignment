const mockRequest = jest.fn()
jest.mock("./request", () => ({
  request: mockRequest,
}))

import * as api from "./api"

describe("api", () => {
  describe("getPosts", () => {
    it("should map passed id in url", () => {
      api.getPost({
        id: 1,
      })

      expect(mockRequest).toBeCalledWith("https://jsonplaceholder.typicode.com/posts/1")
    })
  })

  describe("getPosts", () => {
    it("should map passed parameters in url", () => {
      api.getPosts({
        page: 5,
        limit: 20,
      })

      expect(mockRequest).toBeCalledWith(
        "https://jsonplaceholder.typicode.com/posts?_page=5&_limit=20",
      )
    })
  })

  describe("updatePost", () => {
    it("should put updated information", () => {
      api.updatePost({
        id: 1,
        userId: 10,
        title: "test title",
        body: "test body",
      })

      expect(mockRequest).toBeCalledWith("https://jsonplaceholder.typicode.com/posts/1/", {
        method: "PUT",
        body: '{"id":1,"userId":10,"title":"test title","body":"test body"}',
      })
    })
  })

  describe("createPost", () => {
    it("should post new information", () => {
      api.createPost({
        userId: 10,
        title: "test title",
        body: "test body",
      })

      expect(mockRequest).toBeCalledWith("https://jsonplaceholder.typicode.com/posts/", {
        method: "POST",
        body: '{"userId":10,"title":"test title","body":"test body"}',
      })
    })
  })

  describe("deletePost", () => {
    it("should delte by id", () => {
      api.deletePost({ id: 1 })

      expect(mockRequest).toBeCalledWith("https://jsonplaceholder.typicode.com/posts/1/", {
        method: "DELETE",
      })
    })
  })
})
