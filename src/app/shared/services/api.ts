import { stringify } from "qs"

import { API_ENDPOINT } from "app/shared/config"
import {
  ICreatePostPayload,
  IDeletePostPayload,
  IGetPostPayload,
  IGetPostsPayload,
  IUpdatePostPayload,
} from "app/shared/ducks/post/actions"
import { IPost } from "app/shared/models"
import { IMap } from "app/shared/utils/types"
import { request } from "./request"

export const getPost = (payload: IGetPostPayload) =>
  request<ReadonlyArray<IPost>>(`${API_ENDPOINT}/posts/${payload.id}`)

export const getPosts = (payload: IGetPostsPayload) =>
  request<ReadonlyArray<IPost>>(`${API_ENDPOINT}/posts?${stringify(queryParamsMapper(payload))}`)

export const updatePost = (payload: IUpdatePostPayload) =>
  request<IPost>(`${API_ENDPOINT}/posts/${payload.id}/`, {
    method: "PUT",
    body: JSON.stringify(payload),
  })

export const createPost = (payload: ICreatePostPayload) =>
  request<IPost>(`${API_ENDPOINT}/posts/`, {
    method: "POST",
    body: JSON.stringify(payload),
  })

export const deletePost = (payload: IDeletePostPayload) =>
  request(`${API_ENDPOINT}/posts/${payload.id}/`, {
    method: "DELETE",
  })

const queryParamsMapper = (queryParams: IMap) =>
  Object.keys(queryParams).reduce(
    (acc, key) => ({
      ...acc,
      [key[0] === "_" ? key : `_${key}`]: queryParams[key],
    }),
    {},
  )
