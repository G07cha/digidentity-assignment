export function request<T>(data: string | Request, requestInit?: RequestInit): Promise<T> {
  return fetch(data, requestInit).then((value) => value.json())
}
