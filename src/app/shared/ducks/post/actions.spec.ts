import { getPost } from "app/__mocks__/post.mock"
import * as actions from "./actions"

describe("post.actions", () => {
  describe("getPost", () => {
    it("should return action with given payload", () => {
      const expectedAction = {
        type: actions.Type.GET_POST_REQUEST,
        payload: {
          id: 1,
        },
      }

      expect(actions.getPost({ id: 1 })).toEqual(expectedAction)
    })
  })

  describe("getPostFailure", () => {
    it("should return action with given error", () => {
      const expectedAction = {
        type: actions.Type.GET_POST_FAILURE,
        payload: {
          error: "Test error",
        },
      }

      expect(
        actions.getPostFailure({
          error: "Test error",
        }),
      ).toEqual(expectedAction)
    })
  })

  describe("getPostSuccess", () => {
    it("should return action with received posts", () => {
      const expectedAction = {
        type: actions.Type.GET_POST_SUCCESS,
        payload: getPost(),
      }

      expect(actions.getPostSuccess(getPost())).toEqual(expectedAction)
    })
  })

  describe("getPosts", () => {
    it("should return action with given payload", () => {
      const expectedAction = {
        type: actions.Type.GET_POSTS_REQUEST,
        payload: {
          limit: 10,
          page: 1,
        },
      }

      expect(
        actions.getPosts({
          limit: 10,
          page: 1,
        }),
      ).toEqual(expectedAction)
    })
  })

  describe("getPostsFailure", () => {
    it("should return action with given error", () => {
      const expectedAction = {
        type: actions.Type.GET_POSTS_FAILURE,
        payload: {
          error: "Test error",
        },
      }

      expect(
        actions.getPostsFailure({
          error: "Test error",
        }),
      ).toEqual(expectedAction)
    })
  })

  describe("getPostsSuccess", () => {
    it("should return action with received posts", () => {
      const expectedAction = {
        type: actions.Type.GET_POSTS_SUCCESS,
        payload: [getPost()],
      }

      expect(actions.getPostsSuccess([getPost()])).toEqual(expectedAction)
    })
  })

  describe("updatePost", () => {
    it("should return action with given payload", () => {
      const expectedAction = {
        type: actions.Type.UPDATE_POST_REQUEST,
        payload: getPost(),
      }

      expect(actions.updatePost(getPost())).toEqual(expectedAction)
    })
  })

  describe("updatePostFailure", () => {
    it("should return action with given error", () => {
      const expectedAction = {
        type: actions.Type.UPDATE_POST_FAILURE,
        payload: {
          error: "Test error",
        },
      }

      expect(
        actions.updatePostFailure({
          error: "Test error",
        }),
      ).toEqual(expectedAction)
    })
  })

  describe("updatePostSuccess", () => {
    it("should return action with updated post", () => {
      const expectedAction = {
        type: actions.Type.UPDATE_POST_SUCCESS,
        payload: getPost(),
      }

      expect(actions.updatePostSuccess(getPost())).toEqual(expectedAction)
    })
  })

  describe("createPost", () => {
    it("should return action with given payload", () => {
      const payload = getPost()
      delete payload.id
      const expectedAction = {
        type: actions.Type.CREATE_POST_REQUEST,
        payload,
      }

      expect(actions.createPost(payload)).toEqual(expectedAction)
    })
  })

  describe("createPostFailure", () => {
    it("should return action with given error", () => {
      const expectedAction = {
        type: actions.Type.CREATE_POST_FAILURE,
        payload: {
          error: "Test error",
        },
      }

      expect(
        actions.createPostFailure({
          error: "Test error",
        }),
      ).toEqual(expectedAction)
    })
  })

  describe("createPostSuccess", () => {
    it("should return action with created post", () => {
      const expectedAction = {
        type: actions.Type.CREATE_POST_SUCCESS,
        payload: getPost(),
      }

      expect(actions.createPostSuccess(getPost())).toEqual(expectedAction)
    })
  })

  describe("deletePost", () => {
    it("should return action with given payload", () => {
      const expectedAction = {
        type: actions.Type.DELETE_POST_REQUEST,
        payload: {
          id: getPost().id,
        },
      }

      expect(actions.deletePost({ id: getPost().id })).toEqual(expectedAction)
    })
  })

  describe("deletePostFailure", () => {
    it("should return action with given error", () => {
      const expectedAction = {
        type: actions.Type.DELETE_POST_FAILURE,
        payload: {
          error: "Test error",
        },
      }

      expect(
        actions.deletePostFailure({
          error: "Test error",
        }),
      ).toEqual(expectedAction)
    })
  })

  describe("deletePostSuccess", () => {
    it("should return action with deleted post", () => {
      const expectedAction = {
        type: actions.Type.DELETE_POST_SUCCESS,
        payload: {
          id: getPost().id,
        },
      }

      expect(actions.deletePostSuccess({ id: getPost().id })).toEqual(expectedAction)
    })
  })
})
