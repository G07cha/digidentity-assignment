import { put } from "redux-saga/effects"

import { getPost } from "app/__mocks__/post.mock"
import * as actions from "./actions"
import * as sagas from "./sagas"

describe("post.sagas", () => {
  describe("getPostSaga", () => {
    it("should get post and put it in store", () => {
      const gen = sagas.getPostSaga(actions.getPost({ id: getPost().id }))
      gen.next()

      expect(gen.next(getPost()).value).toEqual(put(actions.getPostSuccess(getPost())))
    })

    it("should put error in case if call failed", () => {
      const gen = sagas.getPostSaga(actions.getPost({ id: getPost().id }))
      gen.next()

      expect(gen.throw!("error").value).toEqual(put(actions.getPostFailure({ error: "error" })))
    })
  })

  describe("getPostsSaga", () => {
    it("should get posts and put them in store", () => {
      const gen = sagas.getPostsSaga(
        actions.getPosts({
          page: 1,
          limit: 10,
        }),
      )
      gen.next()

      expect(gen.next([getPost()]).value).toEqual(put(actions.getPostsSuccess([getPost()])))
    })

    it("should put error in case if call failed", () => {
      const gen = sagas.getPostsSaga(
        actions.getPosts({
          page: 1,
          limit: 10,
        }),
      )
      gen.next()

      expect(gen.throw!("error").value).toEqual(put(actions.getPostsFailure({ error: "error" })))
    })
  })

  describe("updatePostSaga", () => {
    it("should update post and notify store", () => {
      const gen = sagas.updatePostSaga(actions.updatePost(getPost()))
      gen.next()

      expect(gen.next(getPost()).value).toEqual(put(actions.updatePostSuccess(getPost())))
    })

    it("should put error in case if call failed", () => {
      const gen = sagas.updatePostSaga(actions.updatePost(getPost()))
      gen.next()

      expect(gen.throw!("error").value).toEqual(put(actions.updatePostFailure({ error: "error" })))
    })
  })

  describe("createPostSaga", () => {
    it("should create post and notify store", () => {
      const gen = sagas.createPostSaga(actions.createPost(getPost()))
      gen.next()

      expect(gen.next(getPost()).value).toEqual(put(actions.createPostSuccess(getPost())))
    })

    it("should put error in case if call failed", () => {
      const gen = sagas.createPostSaga(actions.createPost(getPost()))
      gen.next()

      expect(gen.throw!("error").value).toEqual(put(actions.createPostFailure({ error: "error" })))
    })
  })

  describe("deletePostSaga", () => {
    it("should delete post and notify store", () => {
      const gen = sagas.deletePostSaga(actions.deletePost({ id: getPost().id }))
      gen.next()

      expect(gen.next().value).toEqual(put(actions.deletePostSuccess({ id: getPost().id })))
    })

    it("should put error in case if call failed", () => {
      const gen = sagas.deletePostSaga(actions.deletePost({ id: getPost().id }))
      gen.next()

      expect(gen.throw!("error").value).toEqual(put(actions.deletePostFailure({ error: "error" })))
    })
  })
})
