import { rootStateMock } from "app/__mocks__/State.mock"
import { getPosts } from "./selectors"

describe("BlogList.selectors", () => {
  describe("getPosts", () => {
    it("should return posts from state", () => {
      expect(getPosts(rootStateMock)).toMatchSnapshot()
    })
  })
})
