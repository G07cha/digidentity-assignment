import { INewPost, IPost } from "app/shared/models"
import { createAction } from "redux-actions"

export enum Type {
  GET_POST_REQUEST = "GET_POST_REQUEST",
  GET_POST_FAILURE = "GET_POST_FAILURE",
  GET_POST_SUCCESS = "GET_POST_SUCCESS",

  GET_POSTS_REQUEST = "GET_POSTS_REQUEST",
  GET_POSTS_FAILURE = "GET_POSTS_FAILURE",
  GET_POSTS_SUCCESS = "GET_POSTS_SUCCESS",

  UPDATE_POST_REQUEST = "UPDATE_POST_REQUEST",
  UPDATE_POST_FAILURE = "UPDATE_POST_FAILURE",
  UPDATE_POST_SUCCESS = "UPDATE_POST_SUCCESS",

  CREATE_POST_REQUEST = "CREATE_POST_REQUEST",
  CREATE_POST_FAILURE = "CREATE_POST_FAILURE",
  CREATE_POST_SUCCESS = "CREATE_POST_SUCCESS",

  DELETE_POST_REQUEST = "DELETE_POST_REQUEST",
  DELETE_POST_FAILURE = "DELETE_POST_FAILURE",
  DELETE_POST_SUCCESS = "DELETE_POST_SUCCESS",
}

/**
 * GET LIST
 */
interface IRequestFailurePayload {
  error: string
}

export interface IGetPostPayload {
  id: number
}
export const getPost = createAction<IGetPostPayload>(Type.GET_POST_REQUEST)

export type IGetPostFailurePayload = IRequestFailurePayload
export const getPostFailure = createAction<IGetPostFailurePayload>(Type.GET_POST_FAILURE)

export type IGetPostSuccessPayload = IPost
export const getPostSuccess = createAction<IGetPostSuccessPayload>(Type.GET_POST_SUCCESS)

/**
 * GET LIST
 */
interface IRequestFailurePayload {
  error: string
}

export interface IGetPostsPayload {
  page: number
  limit: number
}
export const getPosts = createAction<IGetPostsPayload>(Type.GET_POSTS_REQUEST)

export type IGetPostsFailurePayload = IRequestFailurePayload
export const getPostsFailure = createAction<IGetPostsFailurePayload>(Type.GET_POSTS_FAILURE)

export type IGetPostsSuccessPayload = ReadonlyArray<IPost>
export const getPostsSuccess = createAction<IGetPostsSuccessPayload>(Type.GET_POSTS_SUCCESS)

/**
 * UPDATE
 */
export type IUpdatePostPayload = IPost
export const updatePost = createAction<IUpdatePostPayload>(Type.UPDATE_POST_REQUEST)

export type IUpdatePostFailurePayload = IRequestFailurePayload
export const updatePostFailure = createAction<IUpdatePostFailurePayload>(Type.UPDATE_POST_FAILURE)

export type IUpdatePostSuccessPayload = IPost
export const updatePostSuccess = createAction<IUpdatePostSuccessPayload>(Type.UPDATE_POST_SUCCESS)

/**
 * CREATE
 */
export type ICreatePostPayload = INewPost
export const createPost = createAction<ICreatePostPayload>(Type.CREATE_POST_REQUEST)

export type ICreatePostFailurePayload = IRequestFailurePayload
export const createPostFailure = createAction<ICreatePostFailurePayload>(Type.CREATE_POST_FAILURE)

export type ICreatePostSuccess = IPost
export const createPostSuccess = createAction<ICreatePostSuccess>(Type.CREATE_POST_SUCCESS)

/**
 * DELETE
 */
export interface IDeletePostPayload {
  id: number
}
export const deletePost = createAction<IDeletePostPayload>(Type.DELETE_POST_REQUEST)

export type IDeletePostFailurePayload = IRequestFailurePayload
export const deletePostFailure = createAction<IDeletePostFailurePayload>(Type.DELETE_POST_FAILURE)

export interface IDeletePostSuccessPayload {
  id: number
}
export const deletePostSuccess = createAction<IDeletePostSuccessPayload>(Type.DELETE_POST_SUCCESS)
