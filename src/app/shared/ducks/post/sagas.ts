import { Action } from "redux-actions"
import { SagaIterator } from "redux-saga"
import { call, put, takeEvery, takeLatest } from "redux-saga/effects"

import { IPost } from "app/shared/models"
import * as api from "app/shared/services/api"
import * as actions from "./actions"

export function* getPostSaga(action: Action<actions.IGetPostPayload>): SagaIterator {
  try {
    const post: IPost = yield call(api.getPost, action.payload!)

    yield put(actions.getPostSuccess(post))
  } catch (error) {
    yield put(actions.getPostFailure({ error }))
  }
}

export function* getPostsSaga(action: Action<actions.IGetPostsPayload>): SagaIterator {
  try {
    const posts: ReadonlyArray<IPost> = yield call(api.getPosts, {
      page: (action.payload && action.payload.page) || 0,
      limit: (action.payload && action.payload.limit) || 10,
    })

    yield put(actions.getPostsSuccess(posts))
  } catch (error) {
    yield put(actions.getPostsFailure({ error }))
  }
}

export function* updatePostSaga(action: Action<actions.IUpdatePostPayload>): SagaIterator {
  try {
    const updatedPost: IPost = yield call<actions.IUpdatePostPayload>(
      api.updatePost,
      action.payload!,
    )

    if (updatedPost) {
      yield put(actions.updatePostSuccess(action.payload!))
    } else {
      throw new Error("Failed to update post")
    }
  } catch (error) {
    yield put(actions.updatePostFailure({ error }))
  }
}

export function* createPostSaga(action: Action<actions.ICreatePostPayload>): SagaIterator {
  try {
    const createdPost: IPost = yield call<actions.ICreatePostPayload>(
      api.createPost,
      action.payload!,
    )

    if (createdPost) {
      yield put(
        actions.createPostSuccess({
          ...action.payload!,
          id: createdPost.id,
        }),
      )
    } else {
      throw new Error("Failed to create post")
    }
  } catch (error) {
    yield put(actions.createPostFailure({ error }))
  }
}

export function* deletePostSaga(action: Action<actions.IDeletePostPayload>): SagaIterator {
  try {
    yield call<actions.IDeletePostPayload>(api.deletePost, action.payload!)

    yield put(actions.deletePostSuccess(action.payload!))
  } catch (error) {
    yield put(actions.deletePostFailure({ error }))
  }
}

function* rootSaga() {
  yield takeLatest(actions.Type.GET_POSTS_REQUEST, getPostsSaga)
  yield takeEvery(actions.Type.GET_POST_REQUEST, getPostSaga)
  yield takeEvery(actions.Type.UPDATE_POST_REQUEST, updatePostSaga)
  yield takeEvery(actions.Type.CREATE_POST_REQUEST, createPostSaga)
  yield takeEvery(actions.Type.DELETE_POST_REQUEST, deletePostSaga)
}

export default rootSaga
