import { getPost } from "app/__mocks__/post.mock"
import * as actions from "./actions"
import { postsReducer } from "./reducer"

describe("post.reducer", () => {
  it("should return initial state by default", () => {
    expect(postsReducer(undefined as any, {} as any)).toEqual({
      error: "",
      isLoading: false,
      items: [],
    })
  })

  it("should put posts in items when receives GET_POST_SUCCESS", () => {
    expect(
      postsReducer(
        {
          error: "",
          isLoading: true,
          items: [],
        },
        actions.getPostSuccess(getPost()),
      ),
    ).toEqual({
      error: "",
      isLoading: false,
      items: [getPost()],
    })
  })

  it("should put posts in items when receives GET_POSTS_SUCCESS", () => {
    expect(
      postsReducer(
        {
          error: "",
          isLoading: true,
          items: [],
        },
        actions.getPostsSuccess([getPost()]),
      ),
    ).toEqual({
      error: "",
      isLoading: false,
      items: [getPost()],
    })
  })

  it("should add posts in existing list of items when receives GET_POSTS_SUCCESS", () => {
    expect(
      postsReducer(
        {
          error: "",
          isLoading: true,
          items: [getPost()],
        },
        actions.getPostsSuccess([{ ...getPost(), id: 2 }]),
      ),
    ).toEqual({
      error: "",
      isLoading: false,
      items: [getPost(), { ...getPost(), id: 2 }],
    })
  })

  it("should update isLoading and error when receives GET_POSTS_REQUEST", () => {
    expect(
      postsReducer(
        {
          error: "test err",
          isLoading: false,
          items: [],
        },
        actions.getPosts({ page: 1, limit: 10 }),
      ),
    ).toEqual({
      error: "",
      isLoading: true,
      items: [],
    })
  })

  it("should update isLoading and error when receives GET_POSTS_FAILUE", () => {
    expect(
      postsReducer(
        {
          error: "",
          isLoading: true,
          items: [],
        },
        actions.getPostsFailure({ error: "test error" }),
      ),
    ).toEqual({
      error: "test error",
      isLoading: false,
      items: [],
    })
  })

  it("should remove post from state when it deleted", () => {
    expect(
      postsReducer(
        {
          error: "",
          isLoading: false,
          items: [getPost()],
        },
        actions.deletePostSuccess({ id: getPost().id }),
      ),
    ).toEqual({
      error: "",
      isLoading: false,
      items: [],
    })
  })

  it("should update post in state when update is received", () => {
    expect(
      postsReducer(
        {
          error: "",
          isLoading: false,
          items: [getPost()],
        },
        actions.updatePostSuccess({ ...getPost(), title: "New title" }),
      ),
    ).toEqual({
      error: "",
      isLoading: false,
      items: [{ ...getPost(), title: "New title" }],
    })
  })

  it("should add created post to state", () => {
    expect(
      postsReducer(
        {
          error: "",
          isLoading: false,
          items: [],
        },
        actions.createPostSuccess(getPost()),
      ),
    ).toEqual({
      error: "",
      isLoading: false,
      items: [getPost()],
    })
  })
})
