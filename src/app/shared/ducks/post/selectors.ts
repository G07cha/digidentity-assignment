import { RootState } from "app/shared/store/State"

export const getPosts = (state: RootState) => state.posts.items

export const getPost = (state: RootState, id: number) =>
  state.posts.items.find((post) => post.id === id)

export const isLoadingPosts = (state: RootState) => state.posts.isLoading
