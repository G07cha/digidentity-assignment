import { Action } from "redux-actions"

import { IPost } from "app/shared/models/Post"
import { RootState } from "app/shared/store/rootReducer"
import { unique } from "app/shared/utils/array"
import * as actions from "./actions"

const initialState: RootState["posts"] = {
  isLoading: false,
  error: "",
  items: [],
}

export const postsReducer = (
  state: RootState["posts"] = initialState,
  action: Action<
    | actions.IGetPostsSuccessPayload
    | actions.IGetPostsFailurePayload
    | actions.IGetPostsPayload
    | actions.IDeletePostSuccessPayload
    | actions.IGetPostSuccessPayload
    | actions.IUpdatePostSuccessPayload
  >,
) => {
  switch (action.type) {
    case actions.Type.GET_POST_SUCCESS:
      return {
        ...state,
        error: "",
        isLoading: false,
        items: [...state.items, action.payload],
      }

    case actions.Type.GET_POSTS_SUCCESS:
      if (Array.isArray(action.payload)) {
        return {
          ...state,
          error: "",
          isLoading: false,
          items: unique<IPost>([...state.items, ...action.payload], (a, b) => a.id === b.id),
        }
      } else {
        return state
      }

    case actions.Type.GET_POSTS_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: "",
      }

    case actions.Type.GET_POSTS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: (action.payload && (action.payload as actions.IGetPostsFailurePayload).error) || "",
      }

    case actions.Type.DELETE_POST_SUCCESS:
      return {
        ...state,
        items: state.items.filter(
          (item) => item.id !== (action.payload! as actions.IDeletePostSuccessPayload).id,
        ),
      }

    case actions.Type.UPDATE_POST_SUCCESS:
      return {
        ...state,
        items: state.items.map(
          (item) =>
            item.id === (action.payload! as actions.IUpdatePostSuccessPayload).id
              ? action.payload
              : item,
        ),
      }

    case actions.Type.CREATE_POST_SUCCESS:
      return {
        ...state,
        items: [action.payload, ...state.items],
      }

    default:
      return state
  }
}
