import * as arrayUtils from "./array"

describe("array utils", () => {
  describe("unique", () => {
    it("should filter out duplicate items", () => {
      expect(arrayUtils.unique(["foo", "bar", "foo"])).toEqual(["foo", "bar"])
    })

    it("should use given comparer", () => {
      expect(arrayUtils.unique([{ id: 1 }, { id: 1 }], (a, b) => a.id === b.id)).toEqual([
        { id: 1 },
      ])
    })
  })
})
