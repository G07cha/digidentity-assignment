export function unique<T = any>(arr: T[], comparer = (a: T, b: T) => a === b) {
  return arr.filter((value, index, items) => items.findIndex((v) => comparer(value, v)) === index)
}
