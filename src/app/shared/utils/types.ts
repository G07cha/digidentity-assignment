export type primitive = string | number | boolean | undefined | null
export type DeepReadonly<T> = T extends primitive
  ? T
  : T extends Array<infer U> ? IDeepReadonlyArray<U> : DeepReadonlyObject<T>

export interface IDeepReadonlyArray<T> extends ReadonlyArray<DeepReadonly<T>> {}

export type DeepReadonlyObject<T> = { readonly [P in keyof T]: DeepReadonly<T[P]> }

export interface IMap<T = any> {
  [key: string]: T
}
