import { RootState } from "app/shared/store/rootReducer"

export const rootStateMock: RootState = {
  posts: {
    isLoading: false,
    error: "",
    items: [
      {
        body: "test body",
        id: 1,
        title: "test title",
        userId: 1,
      },
    ],
  },
  router: {} as any,
}
