# Blog demo

[react-redux-webpack-typescript](https://github.com/rokoroku/react-redux-typescript-boilerplate) boilerplate was used for setting up the project.

I've decided to focus more on code quality, testing, maintainability and also experiment with concept of Redux ducks which turned out to be really nice.

## Setup

```bash
npm install
```

## Running

```bash
npm start
```

## Build

```bash
npm run build
```

## Test

```bash
npm test
```
