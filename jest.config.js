module.exports = {
  preset: 'ts-jest',
  rootDir: 'src',
  testEnvironment: 'node',
  testMatch: ['**/**/*.spec.ts?(x)'],
  snapshotSerializers: ["enzyme-to-json/serializer"],
  setupTestFrameworkScriptFile: "<rootDir>/setupEnzyme.ts",
  moduleNameMapper: {
    "^app/(.*)$": "<rootDir>/app/$1",
    "\\.css$": "<rootDir>/app/__mocks__/style.mock.js"
  },
  globals: {
    'ts-jest': {
      diagnostics: {
        ignoreCodes: [151001] // https://github.com/kulshekhar/ts-jest/issues/748
      }
    }
  }
};